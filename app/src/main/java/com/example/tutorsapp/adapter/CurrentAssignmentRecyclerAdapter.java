package com.example.tutorsapp.adapter;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tutorsapp.R;
import com.example.tutorsapp.models.RequestedAssignmentModel;
import com.example.tutorsapp.ui.TuitionAssignmentRequestActivity;

import java.util.List;

public class CurrentAssignmentRecyclerAdapter extends RecyclerView.Adapter<CurrentAssignmentRecyclerAdapter.RequestViewHolder> {
    List<RequestedAssignmentModel> searchResultList;
    final int HEAD = 0, CONTENT = 1;
    Activity activity;

    public CurrentAssignmentRecyclerAdapter(List<RequestedAssignmentModel> list, Activity activity) {
        this.searchResultList = list;
        this.activity = activity;
    }

    @Override
    public int getItemViewType(int position) {
        if(position == 0) {
            return HEAD;
        } else {
            return CONTENT;
        }
    }

    @NonNull
    @Override
    public RequestViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(viewType == HEAD ? R.layout.li_current_request_head : R.layout.li_current_request_content, parent, false);
        return new RequestViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RequestViewHolder holder, int position) {
        if(getItemViewType(position) == HEAD) {

        } else {
            holder.setData(searchResultList.get(position-1));
            holder.tvShowDetails.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    activity.startActivity(new Intent(activity, TuitionAssignmentRequestActivity.class));
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        if(searchResultList == null || searchResultList.size() == 0) {
            return 0;
        }
        return searchResultList.size() + 1;
    }

    static class RequestViewHolder extends RecyclerView.ViewHolder {

        TextView tvDandT;
        TextView tvAssign;
        TextView tvShowDetails;
        TextView tvStatus;

        public RequestViewHolder(@NonNull final View itemView) {
            super(itemView);
            tvDandT = itemView.findViewById(R.id.tvDAndT);
            tvAssign = itemView.findViewById(R.id.tvAssign);
            tvShowDetails = itemView.findViewById(R.id.tvShowDetails);
            tvStatus = itemView.findViewById(R.id.tvStatus);
        }

        void setData(RequestedAssignmentModel result) {
            tvDandT.setText(result.getDate());
            tvAssign.setText(result.getLabelText());
            tvShowDetails.setText(R.string.show_details_text);
            tvStatus.setText(result.getStatus());
        }
    }
}
