package com.example.tutorsapp.helper;

import android.text.TextUtils;
import android.util.Patterns;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Constants {
    public static final String TOTAL_MARKS_ERROR="Total marks should not be less than obtained marks";
    public static final String INSTITUTION_TEXT_ERROR="Please add instutution name";
    public static final String DURATION_TEXT_ERROR="Please add duration";
    public static final String YEAR_TEXT_ERROR="Please add year";
    public static final String FINAL_DEGREE_IMAGE_UPLOADED="Final Degree Image Uploaded";
    public static final String CNIC_FRONT_SUCCESS_MESSAGE="CNIC Front uploaded successfully";
    public static final String CNIC_BACK_SUCCESS_MESSAGE="CNIC Back uploaded successfully";
    public static final String LOGIN_EMPTY_MOBILE_ERROR="Phone number cannot be empty";
    public static final String OTP_ERROR="Please enter otp";
    public static final String ALL_FEILDS_ERROR="Please enter all the feilds";
    public static final String datePassey="datePassey";
    public static final String extraDataPassKey="extraDataPassKey";

    public static int Mode;

    public static boolean emailValidator(String email) {
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }


    public static boolean phoneRegex(String s) {
        Pattern p = Pattern.compile("^03");
        Matcher m = p.matcher(s);
        return m.find();
    }
}
