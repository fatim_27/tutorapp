package com.example.tutorsapp.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import com.example.tutorsapp.R;
import com.example.tutorsapp.helper.Constants;

public class VerifyOtpActvity extends AppCompatActivity {
    private EditText mobileNumberEv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activty_verify_otp);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);// pretending keyboard from automatically opening
        initView();
    }

    private void initView() {
        Button verifyBtn = findViewById(R.id.verifyBtn);
        mobileNumberEv = findViewById(R.id.mobileNumberEv);

        verifyBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mobileNumberEv.getText() == null || mobileNumberEv.getText().toString().isEmpty())
                    mobileNumberEv.setError(Constants.OTP_ERROR);
                else {
                    Intent intent = new Intent(VerifyOtpActvity.this, WelcomeUserActivity.class);
                    startActivity(intent);
                }
            }
        });
    }
}
