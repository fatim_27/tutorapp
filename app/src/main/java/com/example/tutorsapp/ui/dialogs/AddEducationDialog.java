package com.example.tutorsapp.ui.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.example.tutorsapp.R;
import com.example.tutorsapp.models.EducationDetailModel;
import com.example.tutorsapp.models.EducationModel;
import com.example.tutorsapp.ui.customview.CustomSpinnerAdapter;

import java.util.ArrayList;
import java.util.Arrays;

public class AddEducationDialog extends Dialog implements View.OnClickListener, AdapterView.OnItemSelectedListener {
    Spinner educationSpinner, degreeSpinner, gradesSpinner;
    EditText instituteEd, cityBoradUni;
    CustomSpinnerAdapter educationAdapter, degreeAdapter, gradersAdapter;
    GetEducation education;
    Button saveEducationBtn;

    public AddEducationDialog(@NonNull Context context, GetEducation getEducation) {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (getWindow() != null) {
            getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            this.getWindow().setSoftInputMode(
                    WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        }
        setCancelable(true);
        setContentView(R.layout.li_alert_add_education);
        educationSpinner = findViewById(R.id.addEducationSp);
        degreeSpinner = findViewById(R.id.degreeSp);
        gradesSpinner = findViewById(R.id.gradeSp);
        instituteEd = findViewById(R.id.institutionNameEv);
        cityBoradUni = findViewById(R.id.universityEv);
        saveEducationBtn = findViewById(R.id.saveBtn);
        saveEducationBtn.setOnClickListener(this);
        educationAdapter = new CustomSpinnerAdapter(context, R.id.spinner_item_tv, R.id.spinner_item_tv, new ArrayList<String>(Arrays.asList(context.getResources().getStringArray(R.array.educationSpinner))));
        degreeAdapter = new CustomSpinnerAdapter(context, R.id.spinner_item_tv, R.id.spinner_item_tv, new ArrayList<String>(Arrays.asList(context.getResources().getStringArray(R.array.degree10thSpinner))));
        gradersAdapter = new CustomSpinnerAdapter(context, R.id.spinner_item_tv, R.id.spinner_item_tv, new ArrayList<String>(Arrays.asList(context.getResources().getStringArray(R.array.gradeSpinner))));
        educationSpinner.setAdapter(educationAdapter);
        degreeSpinner.setAdapter(degreeAdapter);
        gradesSpinner.setAdapter(gradersAdapter);
        educationSpinner.setOnItemSelectedListener(this);
        education = getEducation;


    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.saveBtn) {
            if (!instituteEd.getText().toString().isEmpty() && !cityBoradUni.getText().toString().isEmpty()) {
                EducationDetailModel educationDetails = new EducationDetailModel();
                educationDetails.setDegreeName((String) degreeSpinner.getSelectedItem());
                educationDetails.setEducation((String) educationSpinner.getSelectedItem());
                educationDetails.setGrade((String) gradesSpinner.getSelectedItem());
                educationDetails.setBoardName(cityBoradUni.getText().toString());
                educationDetails.setInstituteName(instituteEd.getText().toString());
                education.education(educationDetails);
                dismiss();
            } else {
                Toast.makeText(getContext(), "Please provide full information", Toast.LENGTH_SHORT).show();
            }

        }
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
        switch (adapterView.getId()) {
            case R.id.addEducationSp:
                if (position == 0) {
                    degreeAdapter.clear();
                    degreeAdapter.addAll(new ArrayList<>(Arrays.asList(getContext().getResources().getStringArray(R.array.degree10thSpinner))));
                    degreeAdapter.notifyDataSetChanged();
                } else if (position == 1) {
                    degreeAdapter.clear();
                    degreeAdapter.addAll(new ArrayList<>(Arrays.asList(getContext().getResources().getStringArray(R.array.degree11thSpinner))));
                    degreeAdapter.notifyDataSetChanged();
                } else if (position == 2) {
                    degreeAdapter.clear();
                    degreeAdapter.addAll(new ArrayList<>(Arrays.asList(getContext().getResources().getStringArray(R.array.degreeGraducationthSpinner))));
                    degreeAdapter.notifyDataSetChanged();
                }
                break;


        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    public interface GetEducation {
        void education(EducationDetailModel educationDetailModel);
    }
}
