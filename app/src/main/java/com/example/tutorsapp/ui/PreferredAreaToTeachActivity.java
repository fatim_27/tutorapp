package com.example.tutorsapp.ui;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tutorsapp.R;
import com.example.tutorsapp.helper.Constants;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.squareup.timessquare.CalendarPickerView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class PreferredAreaToTeachActivity extends AppCompatActivity implements View.OnClickListener {
    private final int ADD_LOCATION_RESULT = 11;
    private final int POINT_OF_REFERENCE_RESULT = 13;
    private Calendar calender;
    private DatePickerDialog.OnDateSetListener date;
    private TextView updateYourAvailabilityStatusTv;
    private TextView areaTv;
    private TextView datesTv;
    private TextView setPointOfReferenceTv;
    private FloatingActionButton addBtn;
    private CalendarPickerView calendar_view;
    private RecyclerView recyclerView;
    private Button saveChangesBtn;

    //for receiving intent info
    private String area;
    private List<String> datesSelected;
    private String dates = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preferred_area_to_teach);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);// pretending keyboard from automatically opening
        initView();
    }

    private void initView() {
        areaTv = findViewById(R.id.areaTv);
        setPointOfReferenceTv = findViewById(R.id.setPointOfReferenceTv);
        setPointOfReferenceTv.setOnClickListener(this);
        updateYourAvailabilityStatusTv = findViewById(R.id.updateYourAvailabilityStatusTv);
        updateYourAvailabilityStatusTv.setOnClickListener(this);
        calendar_view = (CalendarPickerView) findViewById(R.id.calendar_view);
        addBtn = findViewById(R.id.addBtn);
        datesTv = findViewById(R.id.datesTv);
        recyclerView = findViewById(R.id.recyclerView);
        saveChangesBtn = findViewById(R.id.saveChangesBtn);
        saveChangesBtn.setOnClickListener(this);
        addBtn.setOnClickListener(this);
        datesSelected = new ArrayList<>();


        calender = Calendar.getInstance();
        calendar_view.setOnDateSelectedListener(new CalendarPickerView.OnDateSelectedListener() {
            @Override
            public void onDateSelected(Date date) {
                String d;
                int init = 1;
                for (int i = 0; i < 3; i++) {
                    init = date.toString().indexOf(" ", init + 1);
                }
                d = date.toString().substring(0, init);
                datesSelected.add(d);
                if (dates.isEmpty())
                    dates = dates + d;
                else
                    dates = dates + ", " + d;
            }

            @Override
            public void onDateUnselected(Date date) {
                datesSelected.remove(date.toString());
            }
        });

//getting current
        Calendar nextYear = Calendar.getInstance();
        nextYear.add(Calendar.YEAR, 1);
        Date today = new Date();
//add one year to calendar from todays date
        calendar_view.init(today, nextYear.getTime()).inMode(CalendarPickerView.SelectionMode.MULTIPLE);

        final CheckBox time1Cb = findViewById(R.id.time1Cb);
        final CheckBox time2Cb = findViewById(R.id.time2Cb);
        final CheckBox time3Cb = findViewById(R.id.time3Cb);
        final CheckBox time4Cb = findViewById(R.id.time4Cb);
        final CheckBox time5Cb = findViewById(R.id.time5Cb);
        final CheckBox time6Cb = findViewById(R.id.time6Cb);
        final TextView selecTimeTv = findViewById(R.id.selecTimeTv);
        RadioGroup availableTimeRg = findViewById(R.id.availableTimeRg);
        availableTimeRg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.availableRb) {
                    time1Cb.setVisibility(View.VISIBLE);
                    time2Cb.setVisibility(View.VISIBLE);
                    time3Cb.setVisibility(View.VISIBLE);
                    time4Cb.setVisibility(View.VISIBLE);
                    time5Cb.setVisibility(View.VISIBLE);
                    time6Cb.setVisibility(View.VISIBLE);
                    selecTimeTv.setVisibility(View.VISIBLE);
                } else {
                    time1Cb.setVisibility(View.GONE);
                    time2Cb.setVisibility(View.GONE);
                    time3Cb.setVisibility(View.GONE);
                    time4Cb.setVisibility(View.GONE);
                    time5Cb.setVisibility(View.GONE);
                    time6Cb.setVisibility(View.GONE);
                    selecTimeTv.setVisibility(View.GONE);
                }
            }
        });
        //button clicks

        areaTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(PreferredAreaToTeachActivity.this, AddLocationActivity.class);
                startActivityForResult(i, ADD_LOCATION_RESULT);

            }
        });

        //back button
        ImageView backIv = findViewById(R.id.backIv);
        backIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PreferredAreaToTeachActivity.super.onBackPressed();
            }
        });
        //date picker
    }

    private void updateLabel() {
        String myFormat = "MM/dd/yy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        updateYourAvailabilityStatusTv.setText(sdf.format(calender.getTime()));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ADD_LOCATION_RESULT) {
            if (resultCode == Activity.RESULT_OK) {
                String result = data.getStringExtra(Constants.datePassey);
                if (result != null && !result.isEmpty()) {
                    if (areaTv.getText().toString().equals(getResources().getString(R.string.add_location)))
                        areaTv.setText(result);
                    else
                        areaTv.setText(areaTv.getText().toString() + ", " + result);
                }
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        } else if (requestCode == POINT_OF_REFERENCE_RESULT) {
            if (resultCode == Activity.RESULT_OK) {
                String result = data.getStringExtra(Constants.datePassey);
                if (result != null && !result.isEmpty()) {
                    if (setPointOfReferenceTv.getText().toString().equals(getResources().getString(R.string.set_your_point_of_reference)))
                        setPointOfReferenceTv.setText(result);
                    else
                        setPointOfReferenceTv.setText(setPointOfReferenceTv.getText().toString() + ", " + result);
                }
            }
            if (resultCode == Activity.RESULT_CANCELED) {

            }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.setPointOfReferenceTv:
                Intent i = new Intent(PreferredAreaToTeachActivity.this, SelectPointOfReferenceActivity.class);
                startActivityForResult(i, POINT_OF_REFERENCE_RESULT);
                break;
            case R.id.updateYourAvailabilityStatusTv:
                calendar_view.setVisibility(View.VISIBLE);
                addBtn.setVisibility(View.VISIBLE);
                break;
            case R.id.addBtn:
                calendar_view.setVisibility(View.GONE);
                addBtn.setVisibility(View.GONE);
                datesTv.setVisibility(View.VISIBLE);
                datesTv.setText(dates);
             /*   recyclerView.setVisibility(View.VISIBLE);
                SelectedDatedRecyclerAdapter adapter = new SelectedDatedRecyclerAdapter(datesSelected);
                recyclerView.setLayoutManager(new LinearLayoutManager(this));
                recyclerView.setAdapter(adapter);*/
                break;
            case R.id.saveChangesBtn:
                if (validateInput()) {
                    Intent intent = new Intent(PreferredAreaToTeachActivity.this, RegistrationFeeActivity.class);
                    startActivity(intent);
                } else
                    Toast.makeText(this, Constants.ALL_FEILDS_ERROR, Toast.LENGTH_LONG).show();
                break;
        }
    }

    private boolean validateInput() {
        if (areaTv == null)
            return false;
        if (areaTv.getText().toString().equalsIgnoreCase(getResources().getString(R.string.add_location)))
            return false;
        if (setPointOfReferenceTv == null)
            return false;
        if (setPointOfReferenceTv.getText().toString().equalsIgnoreCase(getResources().getString(R.string.set_your_point_of_reference)))
            return false;
        if (datesTv.getVisibility() == View.GONE)
            return false;
        return true;
    }
}