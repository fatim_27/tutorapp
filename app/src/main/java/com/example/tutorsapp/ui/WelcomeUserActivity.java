package com.example.tutorsapp.ui;

import android.content.Intent;
import android.graphics.drawable.ClipDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.example.tutorsapp.R;
import com.example.tutorsapp.helper.Constants;

public class WelcomeUserActivity extends AppCompatActivity {
    private ImageView searchIv;
    private ImageView logoutIv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome_user);
        initView();
    }

    private void initView() {
        searchIv = findViewById(R.id.searchIv);
        logoutIv=findViewById(R.id.logoutIv);

        //adjusting top image of the screen
        ImageView drawerIv = findViewById(R.id.drawerIv);
        ImageView topIv = findViewById(R.id.topIv);
        ClipDrawable mImageDrawable = (ClipDrawable) topIv.getDrawable();
        mImageDrawable.setLevel(10000);
        drawerIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WelcomeUserActivity.this.startActivity(new Intent(WelcomeUserActivity.this, DashboardActivity.class));
            }
        });

        CardView teacherCv = findViewById(R.id.teacherCv);
        teacherCv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Constants.Mode = 0;
                Intent intent = new Intent(WelcomeUserActivity.this, EditProfileActivity.class);
                startActivity(intent);
            }
        });

        CardView quranTeacherCv = findViewById(R.id.quranTeacherCv);
        quranTeacherCv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Constants.Mode = 1;
                Intent intent = new Intent(WelcomeUserActivity.this, EditProfileActivity.class);
                startActivity(intent);
            }
        });

        CardView academyInstitutionCv = findViewById(R.id.academyInstitutionCv);
        academyInstitutionCv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Constants.Mode = 2;
                Intent intent = new Intent(WelcomeUserActivity.this, EditProfileActivity.class);
                startActivity(intent);
            }
        });

        searchIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(WelcomeUserActivity.this, SearchActivity.class);
                startActivity(intent);
            }
        });
        logoutIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(WelcomeUserActivity.this, LoginActivity.class);
                startActivity(intent);
            }
        });

    }
}
