package com.example.tutorsapp.ui;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ClipDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tutorsapp.R;
import com.example.tutorsapp.adapter.CurrentAssignmentRecyclerAdapter;
import com.example.tutorsapp.models.EducationDetailModel;
import com.example.tutorsapp.models.RequestedAssignmentModel;
import com.google.android.material.navigation.NavigationView;

public class DashboardActivity extends BaseActivity implements View.OnClickListener {

    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    private ImageView topIv;
    private ImageView drawerIv;
    private ImageView searchIv;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        drawerLayout = findViewById(R.id.drawer_layout);
        searchIv = findViewById(R.id.searchIv);
        drawerIv = findViewById(R.id.drawerIv);
        RecyclerView rvRequest = findViewById(R.id.rvAssignRequests);
        Button shareDetailBtn = findViewById(R.id.shareDetailBtn);
        shareDetailBtn.setOnClickListener(this);
        rvRequest.setLayoutManager(new LinearLayoutManager(this));
        rvRequest.setAdapter(new CurrentAssignmentRecyclerAdapter(RequestedAssignmentModel.getModels(), this));
        navigationView = (NavigationView) findViewById(R.id.navigation_view);
        topIv = findViewById(R.id.topIv);

        ClipDrawable mImageDrawable = (ClipDrawable) topIv.getDrawable();
        mImageDrawable.setLevel(10000);
        topIv.setOnClickListener(this);

        findViewById(R.id.tvProfileNote).setOnClickListener(this);
        findViewById(R.id.tutionAssignmentTv).setOnClickListener(this);
        findViewById(R.id.assignmentHistoryTv).setOnClickListener(this);
        findViewById(R.id.cancelltedAssignmentTv).setOnClickListener(this);
        searchIv.setOnClickListener(this);
        drawerIv.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        Intent intent;
        switch (view.getId()) {
            case R.id.drawerIv:
                drawerLayout.openDrawer(GravityCompat.START);
                break;
            case R.id.tvProfileNote:
                drawerLayout.closeDrawer(GravityCompat.START);
                break;
            case R.id.tutionAssignmentTv:
                intent = new Intent(DashboardActivity.this, TuitionAssignmentRequestActivity.class);
                startActivity(intent);
                break;
            case R.id.assignmentHistoryTv:
                intent = new Intent(DashboardActivity.this, TuitionAssignmentHistoryActivity.class);
                startActivity(intent);
                break;
            case R.id.cancelltedAssignmentTv:
                intent = new Intent(DashboardActivity.this, CancelledAssignmentActivity.class);
                startActivity(intent);
                break;
            case R.id.searchIv:
                intent = new Intent(DashboardActivity.this, SearchActivity.class);
                startActivity(intent);
                break;
            case R.id.shareDetailBtn:
                showDetailsDialog();
                break;
            default:
                break;
        }
    }


    private void showDetailsDialog() {
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.li_alert_add_notes, null);
        alertDialogBuilder.setView(view);
        alertDialogBuilder.setCancelable(true);

        final AlertDialog dialog = alertDialogBuilder.create();
        final Spinner gradeSp = view.findViewById(R.id.gradeSp);
        final EditText institutionNameEv = view.findViewById(R.id.institutionNameEv);
        final EditText universityEv = view.findViewById(R.id.universityEv);
        final EditText levelEv = view.findViewById(R.id.levelEv);
        final EditText subjectEv = view.findViewById(R.id.subjectEv);
        Button saveBtn = view.findViewById(R.id.saveBtn);

        final EducationDetailModel education = new EducationDetailModel();

        ArrayAdapter<CharSequence> gradeAdapter = ArrayAdapter.createFromResource(this, R.array.youHaving, R.layout.li_spinner);
        gradeAdapter.setDropDownViewResource(R.layout.li_spinner);
        gradeSp.setAdapter(gradeAdapter);
        gradeSp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
//                education.setGrade(gradeSp.getSelectedItem().toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
//                education.setGrade("");
            }
        });

        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }
}
