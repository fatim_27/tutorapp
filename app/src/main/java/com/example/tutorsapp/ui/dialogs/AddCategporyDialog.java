package com.example.tutorsapp.ui.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ExpandableListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tutorsapp.R;
import com.example.tutorsapp.adapter.CategoriesExpendableListAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import androidx.annotation.NonNull;

public class AddCategporyDialog extends Dialog implements View.OnClickListener {
    private CategoriesExpendableListAdapter adapter;
    private ExpandableListView expandableListView;
    private TextView doneTv;
    private GetSubList getSubList;

    public AddCategporyDialog(@NonNull Context context, GetSubList getSubList) {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (getWindow() != null) {
            getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            this.getWindow().setSoftInputMode(
                    WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        }
        setCancelable(true);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(getWindow().getAttributes());
        lp.width = (int) (context.getResources().getDisplayMetrics().widthPixels * 0.90);
        lp.height = (int) (context.getResources().getDisplayMetrics().widthPixels * 0.70);
        getWindow().setAttributes(lp);
        setContentView(R.layout.li_alert_institution_categories);
        expandableListView = findViewById(R.id.categoriesEl);
        adapter = new CategoriesExpendableListAdapter(getContext(), getHeaderlist(), getChildList());
        expandableListView.setAdapter(adapter);
        doneTv = findViewById(R.id.doneBtn);
        doneTv.setOnClickListener(this);
        this.getSubList = getSubList;
    }

    private HashMap<String, List<String>> getChildList() {
        HashMap<String, List<String>> listDataChild = new HashMap<String, List<String>>();
        List<String> oneToEight = new ArrayList<String>();
        oneToEight.add("All");
        oneToEight.add("One");
        oneToEight.add("Two");
        oneToEight.add("Three");
        oneToEight.add("Four");
        oneToEight.add("Five");
        oneToEight.add("Six");

        List<String> nineToTen = new ArrayList<String>();
        nineToTen.add("All");
        nineToTen.add("Nine");
        nineToTen.add("Ten");

        List<String> elevelToTwelve = new ArrayList<String>();
        elevelToTwelve.add("All");
        elevelToTwelve.add("First Year");
        elevelToTwelve.add("Second Year");

        List<String> oALevel = new ArrayList<String>();
        oALevel.add("All");
        oALevel.add("O Level");

        List<String> bComMCom = new ArrayList<String>();
        bComMCom.add("All");
        bComMCom.add("Pak Studies");
        bComMCom.add("Islamiayat");
        bComMCom.add("Urdu");
        bComMCom.add("English");

        listDataChild.put("Grade 1 to 8", oneToEight); // Header, Child data
        listDataChild.put("Grade 9 to 10", nineToTen);
        listDataChild.put("Grade 11 to 12", elevelToTwelve);
        listDataChild.put("O /AS /A Level", oALevel);
        listDataChild.put("B.Com /M.Com", bComMCom);

        return listDataChild;
    }

    private List<String> getHeaderlist() {


        List<String> listDataHeader = new ArrayList<>();
        listDataHeader.add("Grade 1 to 8");
        listDataHeader.add("Grade 9 to 10");
        listDataHeader.add("Grade 11 to 12");
        listDataHeader.add("O /AS /A Level");
        listDataHeader.add("B.Com /M.Com");
        return listDataHeader;
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.doneBtn) {
            if (!adapter.getSubjectTechsHashmap().isEmpty()) {
                getSubList.SubList(adapter.getSubjectTechsHashmap());
                dismiss();
            }else {
                Toast.makeText(getContext(), "Please select Atleast one subject", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public interface GetSubList {
        public void SubList(HashMap<String, List<String>> listHashMap);
    }
}
