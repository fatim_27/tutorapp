package com.example.tutorsapp.ui;

import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tutorsapp.R;
import com.example.tutorsapp.helper.Constants;
import com.example.tutorsapp.models.TeacherModel;
import com.example.tutorsapp.ui.customview.CustomSpinnerAdapter;
import com.mikhaellopez.circularimageview.CircularImageView;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;

//1. user uploads a new image when user

public class EditProfileActivity extends BaseActivity implements View.OnClickListener {
    private static final int PICK_FROM_CAMERA_FRONT = 1;
    private static final int PICK_FROM_GALLARY_FRONT = 2;
    private static final int PICK_FROM_CAMERA_BACK = 3;
    private static final int PICK_FROM_GALLARY_BACK = 4;
    private static final int PICK_FROM_CAMERA_PROFILE = 15;
    private static final int PICK_FROM_GALLARY_PROFILE = 6;

    private boolean isSelectedFront = false;
    private boolean isSelectedBack = false;

    private int permissionAskedFor; //0 for front, 1 for back

    private Button saveBtn;
    private ImageView editProfileImageIv;
    private CircularImageView profile_image;
    private TextView cnicFront;
    private TextView cnicBack;
    private TextView ageTvEt, timeLineTv;
    private EditText contactDetailsEt, emailAddress, firstNameEd, lastNameEd, currentAddressEd, permanentAddressEd;
    private Spinner genderSp, citySp;
    private AlertDialog dialog;

    private String phoneNumber = "";
    private String cnicFrontUri = "", dateOfBirth;
    private String cnicBacktUri = "";
    private Integer citySelected = 0;
    TeacherModel teacherModel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);// pretending keyboard from automatically opening
        initView();
    }

    private void initView() {
        editProfileImageIv = findViewById(R.id.editProfileImageIv);
        profile_image = findViewById(R.id.profile_image);
        cnicBack = findViewById(R.id.cnicBack);
        contactDetailsEt = findViewById(R.id.contactDetailsEt);
        emailAddress = findViewById(R.id.emailAddress);
        genderSp = findViewById(R.id.genderSp);
        citySp = findViewById(R.id.citySp);
        ageTvEt = findViewById(R.id.ageTvEt);
        firstNameEd = findViewById(R.id.firstNameEt);
        lastNameEd = findViewById(R.id.lastNameEt);
        currentAddressEd = findViewById(R.id.currentAddressEt);
        permanentAddressEd = findViewById(R.id.parmanentAddressEt);
        timeLineTv = findViewById(R.id.twentyPercent);
        timeLineTv.setBackground(getDrawable(R.drawable.green_circle));
        timeLineTv.setTextColor(getResources().getColor(R.color.whiteColor));
        cnicFront = findViewById(R.id.cnicFront);
        saveBtn = findViewById(R.id.saveBtn);
        ImageView backIv = findViewById(R.id.backIv);
        if (Constants.Mode == 2) {
            TextView headingTv = findViewById(R.id.headingTv);
            headingTv.setText(getString(R.string.owner_of_acadeny_details));
        }

        CustomSpinnerAdapter adapter = new CustomSpinnerAdapter(EditProfileActivity.this,R.id.spinner_item_tv,R.id.spinner_item_tv,new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.gender))));
        genderSp.setAdapter(adapter);
        CustomSpinnerAdapter citySpinnerAdapter=new CustomSpinnerAdapter(EditProfileActivity.this,R.id.spinner_item_tv,R.id.spinner_item_tv,new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.city))));
        citySp.setAdapter(citySpinnerAdapter);


        //initialiation
        saveBtn.setOnClickListener(this);
        cnicFront.setOnClickListener(this);
        cnicBack.setOnClickListener(this);
        editProfileImageIv.setOnClickListener(this);
        backIv.setOnClickListener(this);
        ageTvEt.setOnClickListener(this);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == 0) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                //if permission granted
                showAlertDialog(permissionAskedFor);
            }
        }
    }

    private void showRemovePhotoAlertDialog(final int cnicSide) { // 0 for front, 1 for back
        AlertDialog alertDialog = new AlertDialog.Builder(EditProfileActivity.this).create();
        alertDialog.setTitle("Remove Photo");
        alertDialog.setCancelable(true);
        alertDialog.setMessage("Are you sure you want to remove the photo?");
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (cnicSide == 0) {
                            cnicFront.setText(getResources().getText(R.string.upload_cnic_front));
                            cnicFront.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, R.drawable.cnic_icon, 0);
                            isSelectedFront = false;
                        } else {
                            cnicBack.setText(getResources().getText(R.string.upload_cnic_back));
                            cnicBack.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, R.drawable.cnic_icon, 0);
                            isSelectedBack = false;
                        }
                        dialog.dismiss();
                    }
                });

        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    private void showAlertDialog(final int cnicSide) { // 0 for front, 1 for back
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.li_capture_image_alert, null);
        alertDialogBuilder.setView(view);
        alertDialogBuilder.setCancelable(true);

        dialog = alertDialogBuilder.create();
        TextView galleryTv = view.findViewById(R.id.galleryTv);
        TextView captureTv = view.findViewById(R.id.captureTv);
        if (galleryTv != null)
            galleryTv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (cnicSide == 0) {
                        Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(pickPhoto, PICK_FROM_GALLARY_FRONT);
                    } else if (cnicSide == 1) {
                        Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(pickPhoto, PICK_FROM_GALLARY_BACK);
                    } else {
                        Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(pickPhoto, PICK_FROM_GALLARY_PROFILE);
                    }
                    dialog.dismiss();

                }
            });

        if (captureTv != null)
            captureTv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (cnicSide == 0) {
                        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(intent, PICK_FROM_CAMERA_FRONT);
                    } else if (cnicSide == 1) {
                        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(intent, PICK_FROM_CAMERA_BACK);
                    } else {
                        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(intent, PICK_FROM_CAMERA_PROFILE);
                    }
                }
            });

        dialog.show();
    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case PICK_FROM_CAMERA_FRONT:
                if (resultCode == Activity.RESULT_OK) {
                    dialog.dismiss();
                    isSelectedFront = true;
                    cnicFront.setText(Constants.CNIC_FRONT_SUCCESS_MESSAGE);
                    cnicFront.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, R.drawable.tick, 0);


                /*    try {
                        Uri uri = data.getData();

                        // Adding captured image in bitmap.
                        bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);

                        // adding captured image in imageview.
                        //     ImageViewHolder.setImageBitmap(bitmap);

                    } catch (IOException e) {

                        e.printStackTrace();
                    }*/
                }

                break;

            case PICK_FROM_GALLARY_FRONT:
                if (resultCode == Activity.RESULT_OK) {
                    cnicFront.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, R.drawable.tick, 0);
                    dialog.dismiss();
                    //pick image from gallery
                    Uri selectedImage = data.getData();
                    String[] filePathColumn = {MediaStore.Images.Media.DATA};
                    cnicFront.setText(Constants.CNIC_FRONT_SUCCESS_MESSAGE);
                    isSelectedFront = true;  // to close the dialog box if image is selected


                    // Get the cursor
                    /*if (selectedImage != null) {
                        Cursor cursor = this.getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                        // Move to first row
                        cursor.moveToFirst();

                        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                        String imgDecodableString = cursor.getString(columnIndex);
                        cursor.close();
                        bitmap = BitmapFactory.decodeFile(imgDecodableString);
                    }*/


                }
                break;
            case PICK_FROM_GALLARY_BACK:
                if (resultCode == Activity.RESULT_OK) {
                    cnicBack.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, R.drawable.tick, 0);
                    dialog.dismiss();
                    //pick image from gallery
                    cnicBack.setText(Constants.CNIC_BACK_SUCCESS_MESSAGE);
                    isSelectedBack = true;  // to close the dialog box if image is selected


                }
                break;
            case PICK_FROM_CAMERA_BACK:
                if (resultCode == Activity.RESULT_OK) {
                    dialog.dismiss();
                    isSelectedBack = true;  // to close the dialog box if image is selected
                    cnicBack.setText(Constants.CNIC_BACK_SUCCESS_MESSAGE);
                    cnicBack.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, R.drawable.tick, 0);
                }

                break;
            case PICK_FROM_GALLARY_PROFILE:
                if (resultCode == Activity.RESULT_OK) {
                    dialog.dismiss();
                    try {
                        final Uri imageUri = data.getData();
                        final InputStream imageStream = getContentResolver().openInputStream(imageUri);
                        final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                        profile_image.setImageBitmap(selectedImage);

                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                        Toast.makeText(EditProfileActivity.this, "Something went wrong", Toast.LENGTH_LONG).show();
                    }
                    //pick image from gallery  cnicBack.setText(Constants.CNIC_PROFILE_SUCCESS_MESSAGE);
                }
                break;
            case PICK_FROM_CAMERA_PROFILE:
                if (resultCode == Activity.RESULT_OK) {
                    dialog.dismiss();
                    Bundle extras = data.getExtras();
                    if (extras != null) {
                        Bitmap imageBitmap = (Bitmap) extras.get("data");
                        profile_image.setImageBitmap(imageBitmap);
                    } else {
                        Toast.makeText(this, "Image cannot be updated", Toast.LENGTH_LONG).show();
                    }


                }
                break;
        }
    }

    public void takePicture(View view) {
        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                new AlertDialog.Builder(EditProfileActivity.this)
                        .setTitle("Select")
                        .setMessage("Are you sure you want to delete this entry?")

                        // Specifying a listener allows you to take an action before dismissing the dialog.
                        // The dialog is automatically dismissed when a dialog button is clicked.
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // Continue with delete operation
                            }
                        })

                        // A null listener allows the button to dismiss the dialog and take no further action.
                        .setNegativeButton(android.R.string.no, null)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
                startActivityForResult(galleryIntent, PICK_FROM_GALLARY_FRONT);
            }
        });

    }


    @Override
    public void onClick(View view) {
        Intent intent;
        switch (view.getId()) {
            case R.id.saveBtn:
                //        phoneNumber = ccp.getFullNumber() + contactDetailsEt.getText().toString();
                if (validateInput()) {
                    if (Constants.Mode == 2) {
                        intent = new Intent(EditProfileActivity.this, AcademyInformationActivity.class);
                        startActivity(intent);
                    } else {
                        addTeacherBasicInfo();
                    }
                }
                break;
            case R.id.backIv:
                EditProfileActivity.super.onBackPressed();
                break;
            case R.id.editProfileImageIv:
                if (ContextCompat.checkSelfPermission(EditProfileActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    permissionAskedFor = 1;
                    ActivityCompat.requestPermissions(EditProfileActivity.this, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
                } else {
                    showAlertDialog(2);
                }
                break;
            case R.id.cnicBack:
                if (ContextCompat.checkSelfPermission(EditProfileActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    permissionAskedFor = 1;
                    ActivityCompat.requestPermissions(EditProfileActivity.this, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
                } else {
                    if (!isSelectedBack)
                        showAlertDialog(1);
                    else
                        showRemovePhotoAlertDialog(1);
                }
                break;

            case R.id.cnicFront:
                if (ContextCompat.checkSelfPermission(EditProfileActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    permissionAskedFor = 0;
                    ActivityCompat.requestPermissions(EditProfileActivity.this, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
                } else {
                    if (!isSelectedFront)
                        showAlertDialog(0);
                    else
                        showRemovePhotoAlertDialog(0);
                }
                break;

            case R.id.ageTvEt:
                final Calendar newCalendar = Calendar.getInstance();
                DatePickerDialog datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        Calendar newDate = Calendar.getInstance();
                        newDate.set(year, monthOfYear, dayOfMonth);
                        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
                        dateOfBirth = inputFormat.format(newDate.getTime());
                        ageTvEt.setText(dayOfMonth + " / " + monthOfYear + " / " + year);
                    }

                }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.show();

                break;
        }
    }

    private boolean validateInput() {

        if (firstNameEd.getText() == null || firstNameEd.getText().toString().isEmpty()) {
            firstNameEd.setError("Please enter first name");
            firstNameEd.requestFocus();
            return false;
        } else if (lastNameEd.getText() == null || lastNameEd.getText().toString().isEmpty()) {
            lastNameEd.setError("Please enter last name");
            lastNameEd.requestFocus();
            return false;
        } else if (ageTvEt.getText() == null || ageTvEt.getText().toString().isEmpty()) {
            showToastBar("Please select age as per cnic", this);
            return false;
        } else if (citySp.getSelectedItemPosition() == 0) {
            showToastBar("Please select city", this);
            return false;
        } else if (contactDetailsEt.getText().toString().isEmpty()) {
            contactDetailsEt.setError("Please enter contact number");
            contactDetailsEt.requestFocus();
            return false;
        } else if (contactDetailsEt.length() > 11 || !Constants.phoneRegex(contactDetailsEt.getText().toString())) {
            contactDetailsEt.setError("Please enter valid contact number");
            contactDetailsEt.requestFocus();
            return false;
        } else if (emailAddress.getText() == null || emailAddress.getText().toString().isEmpty() || !Constants.emailValidator(emailAddress.getText().toString())) {
            emailAddress.setError("Please enter valid email address");
            emailAddress.requestFocus();
            return false;
        } else if (currentAddressEd.getText() == null || currentAddressEd.getText().toString().isEmpty()) {
            currentAddressEd.setError("Please enter current address");
            currentAddressEd.requestFocus();
            return false;
        } else if (permanentAddressEd.getText() == null || permanentAddressEd.getText().toString().isEmpty()) {
            permanentAddressEd.setError("Please enter parmanent address");
            permanentAddressEd.requestFocus();
            return false;
        } else if (!isSelectedFront) {
            showToastBar("Please add front cnic picture", this);
            return false;
        } else if (!isSelectedBack) {
            showToastBar("Please add back cnic picture", this);
            return false;
        } else {
            firstNameEd.setError(null);
            lastNameEd.setError(null);
            ageTvEt.setError(null);
            currentAddressEd.setError(null);
            emailAddress.setError(null);
            permanentAddressEd.setError(null);
            contactDetailsEt.setError(null);
            teacherModel = new TeacherModel();
            teacherModel.setFName(firstNameEd.getText().toString());
            teacherModel.setLName(lastNameEd.getText().toString());
            teacherModel.setDob(dateOfBirth);
            teacherModel.setCurrentAddress(currentAddressEd.getText().toString());
            teacherModel.setPermanentAddress(permanentAddressEd.getText().toString());
            teacherModel.setContactNo(contactDetailsEt.getText().toString());
            teacherModel.setGender(genderSp.getSelectedItemId() == 1 ? "Male" : "Female");
            teacherModel.setCityID(citySp.getSelectedItemPosition());
            teacherModel.setTeacherType(Constants.Mode);
            teacherModel.setEmail(emailAddress.getText().toString());
            teacherModel.setUserInfo(getUserInfo());

            return true;
        }
    }


    private void addTeacherBasicInfo() {

        startActivity(new Intent(EditProfileActivity.this,EducationalDetailsActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK));

       /* final Dialog dialog = DialogHelper.showLoadingDialog(this);
        Call<MessagesResponse> messagesResponseCall = TutorApp.service.addTeacherBasicInfo("application/json", teacherModel);
        BaseCallService.makeServiceCall(messagesResponseCall, new GeneralCallback<Response, Throwable>() {
            @Override
            public void successCall(Response o) {
                DialogHelper.dismiss(dialog);
                if (o.isSuccessful()) {
                    MessagesResponse messagesResponse = (MessagesResponse) o.body();
                    if (messagesResponse.getIsSuccess()) {
                        Intent intent = new Intent(EditProfileActivity.this, EducationalDetailsActivity.class);
                        startActivity(intent);
                    } else {
                        DialogHelper.showMessageDialog(EditProfileActivity.this, "Error", messagesResponse.getMessage());
                    }
                } else {
                    DialogHelper.showMessageDialog(EditProfileActivity.this, "Error", o.message());
                }
            }

            @Override
            public void errorCall(Throwable o) {
                DialogHelper.dismiss(dialog);
                DialogHelper.showMessageDialog(EditProfileActivity.this, "Error", o.getMessage());
            }
        });*/
    }


}