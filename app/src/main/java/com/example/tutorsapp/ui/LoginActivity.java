package com.example.tutorsapp.ui;

import android.content.Intent;
import android.graphics.drawable.ClipDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.tutorsapp.R;
import com.example.tutorsapp.helper.Constants;
import com.hbb20.CountryCodePicker;

public class LoginActivity extends AppCompatActivity {

    private EditText mobileNumberEv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);// pretending keyboard from automatically opening
        initView();
    }

    private void initView() {
        //adjusting top image of the screen
        mobileNumberEv = findViewById(R.id.mobileNumberEv);
        ImageView topIv = findViewById(R.id.topIv);
        ClipDrawable mImageDrawable = (ClipDrawable) topIv.getDrawable();
        mImageDrawable.setLevel(10000);
        Button loginBtn = findViewById(R.id.loginBtn);
        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (validate()) {
                    startActivity(new Intent(LoginActivity.this, VerifyOtpActvity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                }
            }
        });
    }

    private boolean validate() {
        if (mobileNumberEv.getText().toString().isEmpty()) {
            mobileNumberEv.setError("Please enter number");
            mobileNumberEv.requestFocus();
            return false;
        } else if (mobileNumberEv.getText().toString().length() > 11 || !Constants.phoneRegex(mobileNumberEv.getText().toString())) {
            mobileNumberEv.setError("Please enter valid number");
            mobileNumberEv.requestFocus();
            return false;
        } else {
            mobileNumberEv.setError(null);
            return true;
        }
    }
}
