package com.example.tutorsapp.ui;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import androidx.appcompat.app.AppCompatActivity;

import com.example.tutorsapp.GeneralCallback;
import com.example.tutorsapp.R;
import com.example.tutorsapp.TutorApp;
import com.example.tutorsapp.helper.DialogHelper;
import com.example.tutorsapp.models.AccountDetails;
import com.example.tutorsapp.models.GeneralResponse;
import com.example.tutorsapp.network.BaseCallService;
import com.example.tutorsapp.ui.customview.CustomSpinnerAdapter;
import com.example.tutorsapp.utils.Validations;

import java.util.ArrayList;
import java.util.Arrays;

import retrofit2.Call;
import retrofit2.Response;

public class AccountDetailsActivity extends BaseActivity {
    EditText accountTitleTv, accountNumberTv, branchCodeTv;
    View cityRl, servicProviderRl;
    int accountType = 0;
    private Spinner serviceProviderSP;
    private CustomSpinnerAdapter serviceProvideAdapter;
    private AccountDetails accountDetails;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_details);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);// pretending keyboard from automatically opening
        initView();
    }

    private void initView() {
        Spinner spin = findViewById(R.id.bankAccountSp);
        serviceProviderSP = findViewById(R.id.serviceProviderSP);
        accountTitleTv = findViewById(R.id.accountTitleTv);
        accountNumberTv = findViewById(R.id.accountNumberTv);
        branchCodeTv = findViewById(R.id.branchCodeTv);
        cityRl = findViewById(R.id.cityRl);
        servicProviderRl = findViewById(R.id.servicProviderRl);
        serviceProvideAdapter = new CustomSpinnerAdapter(this, R.id.spinner_item_tv, R.id.spinner_item_tv, new ArrayList<String>(Arrays.asList(getResources().getStringArray(R.array.servi_providers))));
        serviceProviderSP.setAdapter(serviceProvideAdapter);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.bank_names, R.layout.li_spinner);
        adapter.setDropDownViewResource(R.layout.li_spinner);
        spin.setAdapter(adapter);
        spin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                accountType = position;
                updateUI();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parentView) { }
        });

        Spinner citySp = findViewById(R.id.citySp);
        ArrayAdapter<CharSequence> cityAdapter = ArrayAdapter.createFromResource(this, R.array.city_names, R.layout.li_spinner);
        cityAdapter.setDropDownViewResource(R.layout.li_spinner);
        citySp.setAdapter(cityAdapter);
        citySp.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

            }
        });

        Button saveChangesBtn = findViewById(R.id.saveChangesBtn);
        saveChangesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(validate()) {
                    addAccountToServer();
                }
                Intent intent = new Intent(AccountDetailsActivity.this, AvailabilityStatusActivity.class);
                startActivity(intent);
            }
        });
        updateUI();
    }

    private void updateUI() {
        accountTitleTv.setHint(accountType == 0 ? "Account Title" : "Title");
        accountNumberTv.setHint(accountType == 0 ? "Account No" : "Mobile No");
        branchCodeTv.setVisibility(accountType == 0 ? View.VISIBLE : View.GONE);
        servicProviderRl.setVisibility(accountType == 0 ? View.GONE : View.VISIBLE);
        cityRl .setVisibility(accountType == 0 ? View.VISIBLE : View.GONE);
    }

    private boolean validate() {
        if(Validations.isEmpty(accountTitleTv.getText())) {
            accountTitleTv.setError("Please enter account title!");
            accountTitleTv.requestFocus();
            return false;
        } else if(Validations.isEmpty(accountNumberTv.getText())) {
            accountTitleTv.setError("Please enter account" + (accountType == 0 ? " Account Number" :  " Mobile Number!"));
            accountTitleTv.requestFocus();
            return false;
        } else if(Validations.isEmpty(branchCodeTv.getText()) && accountType == 0) {
            branchCodeTv.setError("Please enter account account number");
            branchCodeTv.requestFocus();
            return false;
        }
        accountDetails = new AccountDetails();
        accountDetails.setAccountTitle(accountTitleTv.getText().toString());
        accountDetails.setAccountNo(accountNumberTv.getText().toString());
        accountDetails.setBranchCode(branchCodeTv.getText().toString());
        accountDetails.setUserInfo(getUserInfo());
        return true;
    }

    private void addAccountToServer() {
        final Dialog dialog = DialogHelper.showLoadingDialog(this);
        Call<GeneralResponse> messagesResponseCall = TutorApp.service.addAccountDetails("application/json",
                accountDetails);
        BaseCallService.makeServiceCall(messagesResponseCall, new GeneralCallback<Response, Throwable>() {
            @Override
            public void successCall(Response o) {
                DialogHelper.dismiss(dialog);
                if (o.isSuccessful()) {
                    GeneralResponse messagesResponse = (GeneralResponse) o.body();
                    if (messagesResponse.getIsSuccess()) {
                        Intent intent = new Intent(AccountDetailsActivity.this, EducationalDetailsActivity.class);
                        startActivity(intent);
                    } else {
                        DialogHelper.showMessageDialog(AccountDetailsActivity.this, "Error", messagesResponse.getMessage());
                    }
                } else {
                    DialogHelper.showMessageDialog(AccountDetailsActivity.this, "Error", o.message());
                }
            }

            @Override
            public void errorCall(Throwable o) {
                DialogHelper.dismiss(dialog);
                DialogHelper.showMessageDialog(AccountDetailsActivity.this, "Error", o.getMessage());
            }
        });
    }
}
