package com.example.tutorsapp.ui;

import android.app.TimePickerDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarChangeListener;
import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarFinalValueListener;
import com.crystal.crystalrangeseekbar.widgets.CrystalRangeSeekbar;
import com.example.tutorsapp.R;
import com.example.tutorsapp.helper.Constants;

import java.util.Calendar;

public class AvailabilityStatusActivity extends AppCompatActivity implements View.OnClickListener {
    private TextView fromTv;
    private TextView toTv;
    private Spinner currentEmploymentSp;
    private Button saveChangesBtn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_availability_status);
        initView();
    }

    private void initView() {
        saveChangesBtn = findViewById(R.id.saveChangesBtn);
        saveChangesBtn.setOnClickListener(this);
        currentEmploymentSp = findViewById(R.id.currentEmploymentSp);
        fromTv = findViewById(R.id.fromTv);
        fromTv.setOnClickListener(this);
        toTv = findViewById(R.id.toTv);
        toTv.setOnClickListener(this);
        setData();
    }

    private void setData() {
//Per hour fee seekbar
        final CrystalRangeSeekbar perHourFeeRb = findViewById(R.id.perHourFeeRb);
        final TextView minPerHourFee = findViewById(R.id.minPerHourFee);
        final TextView maxPerHourFee = findViewById(R.id.maxPerHourFee);

        perHourFeeRb.setOnRangeSeekbarChangeListener(new OnRangeSeekbarChangeListener() {
            @Override
            public void valueChanged(Number minValue, Number maxValue) {
                if(minValue.intValue() % 100  == 0) {
                    minPerHourFee.setText(String.valueOf(minValue));
                } else {
                    perHourFeeRb.setMinValue(minValue.intValue() / 100);
                }
                if(maxValue.intValue() % 100  == 0) {
                    maxPerHourFee.setText(String.valueOf(maxValue));
                } else {
                    perHourFeeRb.setMaxValue(maxValue.intValue() / 100);
                }
            }
        });

        perHourFeeRb.setOnRangeSeekbarFinalValueListener(new OnRangeSeekbarFinalValueListener() {
            @Override
            public void finalValue(Number minValue, Number maxValue) {
//                Log.d("CRS=>", String.valueOf(minValue) + " : " + String.valueOf(maxValue));
            }
        });

        //Per month fee seekbar
        final CrystalRangeSeekbar perMonthFeeRb = findViewById(R.id.perMonthFeeRb);
        final TextView minPerMonthFee = findViewById(R.id.minPerMonthFee);
        final TextView maxPerMonthFee = findViewById(R.id.maxPerMonthFee);

        perMonthFeeRb.setOnRangeSeekbarChangeListener(new OnRangeSeekbarChangeListener() {
            @Override
            public void valueChanged(Number minValue, Number maxValue) {
                if(minValue.intValue() % 1000  == 0) {
                    minPerMonthFee.setText(String.valueOf(minValue));
                } else {
                    perMonthFeeRb.setMinValue(minValue.intValue() / 100);
                }
                if(maxValue.intValue() % 1000  == 0) {
                    maxPerMonthFee.setText(String.valueOf(maxValue));
                } else {
                    perMonthFeeRb.setMaxValue(maxValue.intValue() / 100);
                }

            }
        });

        perMonthFeeRb.setOnRangeSeekbarFinalValueListener(new OnRangeSeekbarFinalValueListener() {
            @Override
            public void finalValue(Number minValue, Number maxValue) {
//                Log.d("CRS=>", String.valueOf(minValue) + " : " + String.valueOf(maxValue));
            }
        });

        // checkbox set
        CheckBox mondayCb = findViewById(R.id.mondayCb);
        CheckBox tuesdayCb = findViewById(R.id.tuesdayCb);
        CheckBox wednesdayCCb = findViewById(R.id.wednesdayCb);
        CheckBox thursdayCb = findViewById(R.id.thursdayCb);
        CheckBox fridayCb = findViewById(R.id.fridayCb);
        CheckBox saturdayCb = findViewById(R.id.saturdayCb);
        CheckBox sundayCb = findViewById(R.id.sundayCb);
        mondayCb.setChecked(true);
        tuesdayCb.setChecked(true);
        wednesdayCCb.setChecked(true);
        thursdayCb.setChecked(true);
        fridayCb.setChecked(true);

        //current employment
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.current_employment_spinner_items, R.layout.li_spinner);
        adapter.setDropDownViewResource(R.layout.li_spinner);
        currentEmploymentSp.setAdapter(adapter);
    }

    /**
     * @param val pass 0 for from, 1 for to, to check if  to should not be less than from
     */
    public void showHourPicker(final int val) {
        final Calendar myCalender = Calendar.getInstance();
        final int hour = myCalender.get(Calendar.HOUR_OF_DAY);
        final int minute = myCalender.get(Calendar.MINUTE);

        TimePickerDialog.OnTimeSetListener myTimeListener = new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                if (view.isShown()) {
                    myCalender.set(Calendar.HOUR_OF_DAY, hourOfDay);
                    myCalender.set(Calendar.MINUTE, minute);
                    String time = "am";
                    if (hourOfDay > 12) {  // converting in 12 hour format
                        hourOfDay = hourOfDay - 12;
                        time = "pm";
                    }
                    if (val == 0)
                        if (minute < 10)
                            fromTv.setText("" + hourOfDay + " : " + "0" + minute + " " + time);
                        else
                            fromTv.setText("" + hourOfDay + " : " + "" + minute + " " + time);
                    if (val == 1)
                        if (minute < 10)
                            toTv.setText("" + hourOfDay + " : " + "0" + minute + " " + time);
                        else
                            toTv.setText("" + hourOfDay + " : " + "" + minute + " " + time);

                }
            }
        };
        TimePickerDialog timePickerDialog = new TimePickerDialog(this, android.R.style.Theme_Holo_Light_Dialog_NoActionBar, myTimeListener, hour, minute, false);
        timePickerDialog.setTitle("Choose hour:");
        timePickerDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        timePickerDialog.show();

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.saveChangesBtn:
                if (validateInput()) {
                    Intent intent = new Intent(AvailabilityStatusActivity.this, PreferredAreaToTeachActivity.class);
                    startActivity(intent);
                } else
                    Toast.makeText(this, Constants.ALL_FEILDS_ERROR, Toast.LENGTH_LONG).show();
                break;
            case R.id.toTv:
                showHourPicker(1);  //passing 1 for to
                break;
            case R.id.fromTv:
                showHourPicker(0);  //passing 0 for to
                break;
        }
    }

    private boolean validateInput() {
        if (fromTv == null)
            return false;
        if (fromTv.getText().toString().isEmpty()) {
            fromTv.setFocusable(true);
            fromTv.setError("Please select the availbility time");
            return false;
        }
        if (toTv.getText().toString().isEmpty()) {
            toTv.setFocusable(true);
            toTv.setError("Please select the availbility time");
            return false;
        }
        if (currentEmploymentSp.getSelectedItemId() == 0)
            return false;


        return true;
    }
}
