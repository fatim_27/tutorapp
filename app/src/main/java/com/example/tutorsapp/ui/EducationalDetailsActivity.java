package com.example.tutorsapp.ui;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tutorsapp.R;
import com.example.tutorsapp.adapter.EducationRecyclerAdapter;
import com.example.tutorsapp.adapter.ExperienceRecyclerAdapter;
import com.example.tutorsapp.adapter.SubjectTechAdapter;
import com.example.tutorsapp.adapter.TeachingModeRecyclerAdapter;
import com.example.tutorsapp.helper.Constants;
import com.example.tutorsapp.models.EducationDetailModel;
import com.example.tutorsapp.models.ExperienceDetailModel;
import com.example.tutorsapp.models.TeachingModeModel;
import com.example.tutorsapp.ui.dialogs.AddCategporyDialog;
import com.example.tutorsapp.ui.dialogs.AddEducationDialog;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.github.luizgrp.sectionedrecyclerviewadapter.SectionedRecyclerViewAdapter;

public class EducationalDetailsActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener, View.OnClickListener, EducationRecyclerAdapter.RemoveItem {
    private AlertDialog dialog;
    private static final int PICK_FROM_CAMERA = 1;
    private static final int PICK_FROM_GALLARY = 2;
    private boolean isSelected;
    private TextView uploadYourFinalCerTv;
    private EditText obtMarksEv;
    private EditText totalMarksEv;
    private TextView educationTv;
    private TextView experienceTv;
    private TextView whatCatDoYouEv, timeLineTv, timeLineTv0;
    private EditText onlineIdEv;
    private RecyclerView addEducationRecycler, experienceRV, subjectRecyclerView;
    private List<EducationDetailModel> educationDetailModelList;
    private List<ExperienceDetailModel> experianceDetailModels = new ArrayList<>();
    private EducationRecyclerAdapter educationRecyclerAdapter;
    private SectionedRecyclerViewAdapter sectionedAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_educational_details);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);// pretending keyboard from automatically opening
        initView();
    }

    private void initView() {
        timeLineTv = findViewById(R.id.foutyPercent);
        timeLineTv.setBackgroundResource(R.drawable.green_circle);
        timeLineTv0 = findViewById(R.id.twentyPercent);
        timeLineTv0.setBackgroundResource(R.drawable.green_circle);
        timeLineTv.setTextColor(ContextCompat.getColor(EducationalDetailsActivity.this, R.color.whiteColor));
        timeLineTv0.setTextColor(ContextCompat.getColor(EducationalDetailsActivity.this, R.color.whiteColor));
        uploadYourFinalCerTv = findViewById(R.id.uploadYourFinalCerTv);
/*        obtMarksEv = findViewById(R.id.obtMarksEv);
        totalMarksEv = findViewById(R.id.totalMarksEv);*/

        educationDetailModelList = new ArrayList<>();
        experienceTv = findViewById(R.id.experienceTv);
        whatCatDoYouEv = findViewById(R.id.whatCatDoYouEv);
        onlineIdEv = findViewById(R.id.onlineIdEv);
        addEducationRecycler = findViewById(R.id.recyclerView);
        addEducationRecycler.setLayoutManager(new LinearLayoutManager(this));
        educationRecyclerAdapter = new EducationRecyclerAdapter(educationDetailModelList, this);
        addEducationRecycler.setAdapter(educationRecyclerAdapter);
        subjectRecyclerView = findViewById(R.id.subjectCategoryRecycler);
        subjectRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        experienceRV = findViewById(R.id.experienceRV);
        educationTv = findViewById(R.id.educationTv);
//        educationRl = findViewById(R.id.educationRl);
        educationTv.setOnClickListener(this);
//        educationSp = findViewById(R.id.educationSp);
        Button saveChangesBtn = findViewById(R.id.saveChangesBtn);

        saveChangesBtn.setOnClickListener(this);
        //back button
        ImageView backIv = findViewById(R.id.backIv);
        backIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EducationalDetailsActivity.super.onBackPressed();
            }
        });
        //spinner
//        Spinner teachingModeSp = findViewById(R.id.teachingModeSp);
        findViewById(R.id.onlineIdEv).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showTeachingDialog();
            }
        });

        /*
         CustomSpinnerAdapter customAdapter = new CustomSpinnerAdapter(getApplicationContext(), R.array.teachingModeSpinner);
        spin.setAdapter(customAdapter);
      */
//        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.teachingModeSpinner, R.layout.li_spinner);
//        adapter.setDropDownViewResource(R.layout.li_spinner);

     /*   Spinner educationSp = findViewById(R.id.educationSp);
        if (Constants.Mode == 0) {
            ArrayAdapter<CharSequence> educationAdapter = ArrayAdapter.createFromResource(this, R.array.teacher_education_spinner, R.layout.li_spinner);
            educationAdapter.setDropDownViewResource(R.layout.li_spinner);
            educationSp.setAdapter(educationAdapter);
        } else if (Constants.Mode == 1) {
            ArrayAdapter<CharSequence> educationAdapter = ArrayAdapter.createFromResource(this, R.array.hafize_Quran_Spinner, R.layout.li_spinner);
            educationAdapter.setDropDownViewResource(R.layout.li_spinner);
            educationSp.setAdapter(educationAdapter);
        }
*/
        if (Constants.Mode == 0) {
            findViewById(R.id.educationTv).setVisibility(View.VISIBLE);
//            findViewById(R.id.educationRl).setVisibility(View.GONE);
//            uploadYourFinalCerTv.setVisibility(View.VISIBLE);
        } else if (Constants.Mode == 1) {
            findViewById(R.id.educationTv).setVisibility(View.GONE);
//            uploadYourFinalCerTv.setVisibility(View.GONE);
//            educationRl.setVisibility(View.VISIBLE);
            ArrayAdapter<CharSequence> educationAdapter = ArrayAdapter.createFromResource(this, R.array.hafize_Quran_Spinner, R.layout.li_spinner);
            educationAdapter.setDropDownViewResource(R.layout.li_spinner);
//            educationSp.setAdapter(educationAdapter);
        } else {
            educationTv.setVisibility(View.VISIBLE);
//            educationRl.setVisibility(View.GONE);
            //       uploadYourFinalCerTv.setVisibility(View.VISIBLE);
        }

        uploadYourFinalCerTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ContextCompat.checkSelfPermission(EducationalDetailsActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(EducationalDetailsActivity.this, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
                } else {
                    showDialog();
                }
            }
        });

        //checking total marks should not be less than obt marks
      /*  totalMarksEv.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if (obtMarksEv.getText() != null && !obtMarksEv.getText().toString().isEmpty() && Integer.parseInt(obtMarksEv.getText().toString()) > Integer.parseInt(totalMarksEv.getText().toString()))
                    Toast.makeText(EducationalDetailsActivity.this, Constants.TOTAL_MARKS_ERROR, Toast.LENGTH_LONG).show();
            }
        });*/

       /* obtMarksEv.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (totalMarksEv.getText() != null && !totalMarksEv.getText().toString().isEmpty() && Integer.parseInt(totalMarksEv.getText().toString()) < Integer.parseInt(obtMarksEv.getText().toString()))
                    Toast.makeText(EducationalDetailsActivity.this, Constants.TOTAL_MARKS_ERROR, Toast.LENGTH_LONG).show();
            }
        });*/

        //experience
        experienceTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAddExperienceDialog();
            }
        });
        whatCatDoYouEv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Constants.Mode == 0)
                    showTeacherCategoriesDialog();
                else if (Constants.Mode == 1)
                    showQuranTeacherCategoriesDialog();
            }
        });

    }

    private void showQuranTeacherCategoriesDialog() {
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.li_alert_quran_teacher_catrgories, null);
        alertDialogBuilder.setView(view);
        alertDialogBuilder.setCancelable(true);

        dialog = alertDialogBuilder.create();
        dialog.show();
    }

    private void showDialog() {
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.li_capture_image_alert, null);
        alertDialogBuilder.setView(view);
        alertDialogBuilder.setCancelable(true);

        dialog = alertDialogBuilder.create();
        TextView galleryTv = view.findViewById(R.id.galleryTv);
        TextView captureTv = view.findViewById(R.id.captureTv);
        if (galleryTv != null)
            galleryTv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    loadFromGallery();
                    if (isSelected) {
                        Toast.makeText(EducationalDetailsActivity.this, "Image seleced", Toast.LENGTH_LONG).show();
                        //TODO any indation in the cnicFront Tv
                        dialog.dismiss();
                    }
                }
            });

        if (captureTv != null)
            captureTv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent, PICK_FROM_CAMERA);
                }
            });

        dialog.show();
    }

    private void showAddExperienceDialog() {
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.li_alert_add_experience, null);
        alertDialogBuilder.setView(view);
        alertDialogBuilder.setCancelable(true);

        dialog = alertDialogBuilder.create();
        final EditText institutionName = view.findViewById(R.id.institutionName);
        final TextView fromTv = view.findViewById(R.id.fromTv);
        final TextView toTv = view.findViewById(R.id.toTv);
        final EditText yearEv = view.findViewById(R.id.yearEv);
        Button addBtn = view.findViewById(R.id.addBtn);
        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (institutionName.getText() != null && institutionName.getText().toString().isEmpty())
                    Toast.makeText(EducationalDetailsActivity.this, Constants.INSTITUTION_TEXT_ERROR, Toast.LENGTH_SHORT).show();
                else if (fromTv.getText() != null && fromTv.getText().toString().isEmpty())
                    Toast.makeText(EducationalDetailsActivity.this, Constants.DURATION_TEXT_ERROR, Toast.LENGTH_SHORT).show();
//                else if (yearEv.getText() != null && yearEv.getText().toString().isEmpty())
//                    Toast.makeText(EducationalDetailsActivity.this, Constants.YEAR_TEXT_ERROR, Toast.LENGTH_SHORT).show();
                else {
                    dialog.dismiss();
                    experianceDetailModels.add(new ExperienceDetailModel(institutionName.getText().toString(), fromTv.getText().toString(), toTv.getText().toString(), ""));
                    setExperienceRecyclerAdapter();
                }
            }
        });

        fromTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar cldr = Calendar.getInstance();
                int day = cldr.get(Calendar.DAY_OF_MONTH);
                int month = cldr.get(Calendar.MONTH);
                int year = cldr.get(Calendar.YEAR);
                // date picker dialog
                DatePickerDialog picker = new DatePickerDialog(EducationalDetailsActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                fromTv.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                            }
                        }, year, month, day);
                picker.show();
            }
        });

        toTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar cldr = Calendar.getInstance();
                int day = cldr.get(Calendar.DAY_OF_MONTH);
                int month = cldr.get(Calendar.MONTH);
                int year = cldr.get(Calendar.YEAR);
                // date picker dialog
                DatePickerDialog picker = new DatePickerDialog(EducationalDetailsActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                toTv.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                            }
                        }, year, month, day);
                picker.show();
            }
        });

        dialog.show();
    }


    private void showTeachingDialog() {
        final List<TeachingModeModel> teachingModeModels = getTeachingList();
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.li_alert_add_teaching_mode, null);
        alertDialogBuilder.setView(view);
        alertDialogBuilder.setCancelable(true);
        final RecyclerView teachingModelRV = view.findViewById(R.id.teachingModelRV);
        teachingModelRV.setLayoutManager(new LinearLayoutManager(this));
        TeachingModeRecyclerAdapter teachingModeRecyclerAdapter = new TeachingModeRecyclerAdapter(teachingModeModels);
        teachingModelRV.setAdapter(teachingModeRecyclerAdapter);
        dialog = alertDialogBuilder.create();
        Button saveBtn = view.findViewById(R.id.saveBtn);

        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                if (teachingModeModels.get(1).isChecked() || teachingModeModels.get(2).isChecked()) {
                    onlineIdEv.setVisibility(View.VISIBLE);
                } else {
                    onlineIdEv.setVisibility(View.GONE);
                }
            }
        });
        dialog.show();
    }

    private List<TeachingModeModel> getTeachingList() {
        List<TeachingModeModel> teachingModeModels = new ArrayList<>();
        teachingModeModels.add(new TeachingModeModel("Home", false));
        teachingModeModels.add(new TeachingModeModel("Online", false));
        teachingModeModels.add(new TeachingModeModel("Both", false));
        return teachingModeModels;
    }

    private void showAddEducationialog() {
        new AddEducationDialog(EducationalDetailsActivity.this, new AddEducationDialog.GetEducation() {
            @Override
            public void education(EducationDetailModel educationDetailModel) {
                if (educationDetailModelList != null && educationDetailModel != null) {
                    addEducationRecycler.setVisibility(View.VISIBLE);
                    educationDetailModelList.add(educationDetailModel);
                    educationRecyclerAdapter.setDate(educationDetailModelList);
                }
            }
        }).show();
    }

    private void setExperienceRecyclerAdapter() {
        experienceRV.setVisibility(View.VISIBLE);
        ExperienceRecyclerAdapter adapter = new ExperienceRecyclerAdapter(experianceDetailModels);
        experienceRV.setLayoutManager(new LinearLayoutManager(this));
        experienceRV.setAdapter(adapter);
    }

    private void showTeacherCategoriesDialog() {
       /* final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.li_alert_institution_categories, null);
        alertDialogBuilder.setView(view);
        alertDialogBuilder.setCancelable(true);

        dialog = alertDialogBuilder.create();

        ExpandableListView expListView = (ExpandableListView) view.findViewById(R.id.categoriesEl);

        // preparing list data
        List<String> listDataHeader;
        listDataHeader = new ArrayList<String>();
        HashMap<String, List<String>> listDataChild = new HashMap<String, List<String>>();

        // Adding child data
        listDataHeader.add("Grade 1 to 8");
        listDataHeader.add("Grade 9 to 10");
        listDataHeader.add("Grade 11 to 12");
        listDataHeader.add("O /AS /A Level");
        listDataHeader.add("B.Com /M.Com");

        List<String> oneToEight = new ArrayList<String>();
        oneToEight.add("All");
        oneToEight.add("Subject");
        oneToEight.add("Subject");
        oneToEight.add("Subject");
        oneToEight.add("Subject");
        oneToEight.add("Subject");
        oneToEight.add("Subject");

        List<String> nineToTen = new ArrayList<String>();
        nineToTen.add("All");
        nineToTen.add("Subject");
        nineToTen.add("Subject");
        nineToTen.add("Subject");
        nineToTen.add("Subject");
        nineToTen.add("Subject");
        nineToTen.add("Subject");

        List<String> elevelToTwelve = new ArrayList<String>();
        elevelToTwelve.add("All");
        elevelToTwelve.add("Subject");
        elevelToTwelve.add("Subject");
        elevelToTwelve.add("Subject");
        elevelToTwelve.add("Subject");
        elevelToTwelve.add("Subject");
        elevelToTwelve.add("Subject");

        List<String> oALevel = new ArrayList<String>();
        oALevel.add("All");
        oALevel.add("Subject");
        oALevel.add("Subject");
        oALevel.add("Subject");
        oALevel.add("Subject");
        oALevel.add("Subject");
        oALevel.add("Subject");

        List<String> bComMCom = new ArrayList<String>();
        bComMCom.add("All");
        bComMCom.add("Subject");
        bComMCom.add("Subject");
        bComMCom.add("Subject");
        bComMCom.add("Subject");
        bComMCom.add("Subject");
        bComMCom.add("Subject");


        listDataChild.put(listDataHeader.get(0), oneToEight); // Header, Child data
        listDataChild.put(listDataHeader.get(1), nineToTen);
        listDataChild.put(listDataHeader.get(2), elevelToTwelve);
        listDataChild.put(listDataHeader.get(3), oALevel);
        listDataChild.put(listDataHeader.get(4), bComMCom);

        CategoriesExpendableListAdapter listAdapter = new CategoriesExpendableListAdapter(this, listDataHeader, listDataChild);
        // setting list adapter
        expListView.setAdapter(listAdapter);

        dialog.show();*/

        new AddCategporyDialog(this, new AddCategporyDialog.GetSubList() {
            @Override
            public void SubList(HashMap<String, List<String>> listHashMap) {
                sectionedAdapter = new SectionedRecyclerViewAdapter();
                for (Map.Entry<String, List<String>> listEntry : listHashMap.entrySet()) {
                    sectionedAdapter.addSection(new SubjectTechAdapter(listEntry.getKey(), listEntry.getValue(), new SubjectTechAdapter.ClickListener() {
                        @Override
                        public void onItemRootViewClicked(@NonNull SubjectTechAdapter section, int itemAdapterPosition) {
                            sectionedAdapter.notifyItemRemoved(itemAdapterPosition);
                        }
                    }));
                }
                subjectRecyclerView.setAdapter(sectionedAdapter);
                subjectRecyclerView.setVisibility(View.VISIBLE);

            }
        }).show();
    }

    private void loadFromGallery() {
        Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(pickPhoto, PICK_FROM_GALLARY);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Bitmap bitmap = null;

        switch (requestCode) {
            case PICK_FROM_CAMERA:
                if (resultCode == Activity.RESULT_OK) {
                    //enable is it your final degree radio boxes
                    uploadYourFinalCerTv.setText(Constants.FINAL_DEGREE_IMAGE_UPLOADED);
                    uploadYourFinalCerTv.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, R.drawable.tick, 0);
                }

                break;
            case PICK_FROM_GALLARY:
                if (resultCode == Activity.RESULT_OK) {
                    uploadYourFinalCerTv.setText(Constants.FINAL_DEGREE_IMAGE_UPLOADED);
                    uploadYourFinalCerTv.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, R.drawable.tick, 0);
                }
                break;
        }
        dialog.dismiss();

    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.educationTv:
                showAddEducationialog();
                break;
            case R.id.saveChangesBtn:
                if (validate()) {
//                    addEducationToServer();
                }
        }
    }


    private boolean validate() {
        // TODO add validation to check every thing is filled or not
        return true;
    }

    @Override
    public void remove(int position) {
        if (position != -1)
            educationDetailModelList.remove(educationDetailModelList.get(position));
    }
}