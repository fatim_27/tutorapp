package com.example.tutorsapp.models;

public class TeachingModeModel {
    private String teachingMethod;
    private boolean isChecked;
    public TeachingModeModel(String teachingMethod, boolean isChecked) {
        this.teachingMethod = teachingMethod;
        this.isChecked = isChecked;
    }

    public TeachingModeModel() {
    }

    public String getTeachingMethod() {
        return teachingMethod;
    }

    public void setTeachingMethod(String teachingMethod) {
        this.teachingMethod = teachingMethod;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }
}
