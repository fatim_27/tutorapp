package com.example.tutorsapp.models;

public class ExperienceDetailModel {
    private String instituteName;
    private String startDate;
    private String endDate;
    private String experienceInYears;

    public ExperienceDetailModel(String instituteName, String startDate, String endDate, String experienceInYears) {
        this.instituteName = instituteName;
        this.startDate = startDate;
        this.endDate = endDate;
        this.experienceInYears = experienceInYears;
    }

    public ExperienceDetailModel() {
    }

    public String getInstituteName() {
        return instituteName;
    }

    public void setInstituteName(String instituteName) {
        this.instituteName = instituteName;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getExperienceInYears() {
        return experienceInYears;
    }

    public void setExperienceInYears(String experienceInYears) {
        this.experienceInYears = experienceInYears;
    }
}
