package com.example.tutorsapp;

import android.app.Application;
import android.content.Context;

import com.example.tutorsapp.network.ClientInstance;
import com.example.tutorsapp.network.GetDataService;

public class TutorApp extends Application {
    public static GetDataService service;
    public static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        context = this;
        service = ClientInstance.getRetrofitInstance().create(GetDataService.class);
    }

    public static Context getContext() {
        return context;
    }


}
