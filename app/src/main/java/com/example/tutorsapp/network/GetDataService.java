package com.example.tutorsapp.network;

import com.example.tutorsapp.models.AccountDetails;
import com.example.tutorsapp.models.EducationModel;
import com.example.tutorsapp.models.GeneralResponse;
import com.example.tutorsapp.models.TeacherModel;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface GetDataService {

    @POST("/Teacher/AddTeacherBasicInfo")
    Call<GeneralResponse> addTeacherBasicInfo(
            @Header("Content-Type") String contentType,
            @Body TeacherModel body);

    @POST("/Teacher/AddEducationalDetails")
    Call<GeneralResponse> addEducationInfo(
            @Header("Content-Type") String contentType,
            @Body EducationModel body);

    @POST("/Teacher/AddAccountDetails")
    Call<GeneralResponse> addAccountDetails (
            @Header("Content-Type") String contentType,
            @Body AccountDetails body);
}
