package com.example.tutorsapp.network;

import com.example.tutorsapp.GeneralCallback;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BaseCallService<T> {
    public static<V> void makeServiceCall(Call<V> call, final GeneralCallback callback) {
        final CommunicationModel communicationModel = new CommunicationModel();
        call.enqueue(new Callback<V>() {
            @Override
            public void onResponse(Call<V> call, Response<V> response) {
                callback.successCall(response);
            }

            @Override
            public void onFailure(Call<V> call, Throwable t) {
                callback.errorCall(t);
            }
        });
    }
}

class CommunicationModel<T> {
    String code;
    String message;
    Boolean isSuccess;
    T data;
}
